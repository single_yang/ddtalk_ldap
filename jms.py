import pymysql
import pinyin
from settings import DB_HOST, DB_NAME, DB_PORT, DB_USER, DB_PASSWORD


def delete_user(name, position):
    position = position.replace('组', '')
    if position not in ['后端', '前端', '测试', '中台']:
        return "Other"
    username = pinyin.get(name, format='strip')
    conn = pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASSWORD, port=DB_PORT, db=DB_NAME)
    cur = conn.cursor()
    try:
        cur.execute('DELETE FROM `users_user` WHERE `username`="%s"' % username)
        conn.commit()
        return "%s已从Jumpserver中删除" % name
    except Exception as e:
        return e
    finally:
        cur.close()
        conn.close()
