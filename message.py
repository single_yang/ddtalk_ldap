from settings import MOBILE_NUMBER, WEBHOOK
import requests
import json


def SendMessage(info):
    message = {
        "msgtype": "markdown",
        "markdown": {
            "title": "变更通知",
            "text": f"#### {info['type']} \n> 姓名: {info['username']} \n 部门: {info['department']} \n 岗位: {info['position']}"
        },
        "at": {
            "atMobiles": [
                MOBILE_NUMBER
            ],
            "isAtAll": False
        }
    }
    headers = {'Content-Type': 'application/json'}
    try:
        requests.post(url=WEBHOOK, data=json.dumps(message), headers=headers)
    except Exception as e:
        print(e)

